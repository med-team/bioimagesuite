//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





/* -- vtkmpjImageTensorInvariants.h 
   
      Marcel Jackowski, November 2002

*/

#ifndef __vtkmpjImageTensorInvariants_h
#define __vtkmpjImageTensorInvariants_h

// Invariant options
#define VTK_MPJ_TENSORINV_ALL   0    
#define VTK_MPJ_TENSORINV_FA    1
#define VTK_MPJ_TENSORINV_RA    2
#define VTK_MPJ_TENSORINV_VR    3
#define VTK_MPJ_TENSORINV_MD    4
#define VTK_MPJ_TENSORINV_SK    5

#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkFloatArray.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

class vtkmpjImageTensorInvariants : public vtkpxSimpleImageToImageFilter
{
 public:
  static vtkmpjImageTensorInvariants *New();
  vtkTypeMacro(vtkmpjImageTensorInvariants,vtkpxSimpleImageToImageFilter);
  
  // Description: 
  // Sets/Gets mask for determining the extents for invariants
  // computation. If a mask is not provided, invariants are
  // computed for every voxel in the image.
  vtkSetObjectMacro(Mask, vtkImageData);
  vtkGetObjectMacro(Mask, vtkImageData);

  // Description:
  // Set/Get the Operation to perform.
  vtkSetMacro(Operation,int);
  vtkGetMacro(Operation,int);

  void SetOperationToFractionalAnisotropy() {this->SetOperation(VTK_MPJ_TENSORINV_FA);};
  void SetOperationToRelativeAnisotropy() {this->SetOperation(VTK_MPJ_TENSORINV_RA);};
  void SetOperationToVolumeRatio() {this->SetOperation(VTK_MPJ_TENSORINV_VR);};
  void SetOperationToMeanDiffusivity() {this->SetOperation(VTK_MPJ_TENSORINV_MD);};
  void SetOperationToDefault() {this->SetOperation(VTK_MPJ_TENSORINV_ALL);};

  // Description:
  // Set/Get the value for results falling outside the mask
  vtkSetMacro(MaskOutValue,float);
  vtkGetMacro(MaskOutValue,float);
  
protected:
  int Operation;  /* Invariant to be computed */
  float MaskOutValue; 
  vtkImageData *Mask;
  
  vtkmpjImageTensorInvariants();
  virtual ~vtkmpjImageTensorInvariants();

  virtual void SimpleExecute(vtkImageData *input, vtkImageData *output);
  void ExecuteInformation();

 private:
  vtkmpjImageTensorInvariants(const vtkmpjImageTensorInvariants&); // Not implemented
  void operator=(const vtkmpjImageTensorInvariants&); // Not Implemented
};

#endif

