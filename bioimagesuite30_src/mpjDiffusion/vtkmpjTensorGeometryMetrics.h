//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





/* -- vtkmpjTensorGeometryMetrics.h 
   
   Description: Computes geometrical measures of diffusion (Westin '97).

   Marcel Jackowski, February 2003

*/

#ifndef __vtkmpjTensorGeometryMetrics_h
#define __vtkmpjTensorGeometryMetrics_h

// Invariant options
#define VTK_MPJ_TENSOR_GEOM_ALL 0    
#define VTK_MPJ_TENSOR_GEOM_L   1
#define VTK_MPJ_TENSOR_GEOM_P   2
#define VTK_MPJ_TENSOR_GEOM_S   3

#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkFloatArray.h"
#include "vtkImageData.h"
#include "vtkPointData.h"

class vtkmpjTensorGeometryMetrics : public vtkpxSimpleImageToImageFilter
{
 public:
  static vtkmpjTensorGeometryMetrics *New();
  vtkTypeMacro(vtkmpjTensorGeometryMetrics,vtkpxSimpleImageToImageFilter);
  
  // Description: 
  // Sets/Gets mask for determining the extents for invariants
  // computation. If a mask is not provided, invariants are
  // computed for every voxel in the image.
  vtkSetObjectMacro(Mask, vtkImageData);
  vtkGetObjectMacro(Mask, vtkImageData);

  // Description:
  // Set/Get the Operation to perform.
  vtkSetMacro(Operation,int);
  vtkGetMacro(Operation,int);

  void SetOperationToLinear() {this->SetOperation(VTK_MPJ_TENSOR_GEOM_L);};
  void SetOperationToPlanar() {this->SetOperation(VTK_MPJ_TENSOR_GEOM_P);};
  void SetOperationToSpherical() {this->SetOperation(VTK_MPJ_TENSOR_GEOM_S);};
  void SetOperationToDefault() {this->SetOperation(VTK_MPJ_TENSOR_GEOM_ALL);};

  // Description:
  // Set/Get the value for results falling outside the mask
  vtkSetMacro(MaskOutValue,float);
  vtkGetMacro(MaskOutValue,float);
  
protected:
  int Operation;  /* Invariant to be computed */
  float MaskOutValue; 
  vtkImageData *Mask;
  
 vtkmpjTensorGeometryMetrics();
  virtual ~vtkmpjTensorGeometryMetrics();

  virtual void SimpleExecute(vtkImageData *input, vtkImageData *output);
  void ExecuteInformation();

 private:
 vtkmpjTensorGeometryMetrics(const vtkmpjTensorGeometryMetrics&); // Not implemented
  void operator=(const vtkmpjTensorGeometryMetrics&); // Not Implemented
};

#endif

