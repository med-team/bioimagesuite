//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#ifndef __vtkmpjVectorStreamlineStatistics_h
#define __vtkmpjVectorStreamlineStatistics_h

#include "vtkPolyDataToPolyDataFilter.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDataArray.h"
#include "vtkFloatArray.h"
#include "vtkPointData.h"
#include "vtkPoints.h"

class vtkmpjVectorStreamlineStatistics : public vtkPolyDataToPolyDataFilter
{
public:
  vtkTypeMacro(vtkmpjVectorStreamlineStatistics,vtkPolyDataToPolyDataFilter);

  // Description:
  // Construct object 
  static vtkmpjVectorStreamlineStatistics *New();

  // Get statistics across fibers
  vtkGetMacro(NumberOfFibers, int);

  vtkGetMacro(Minimum, float);
  vtkGetMacro(Maximum, float);
  vtkGetMacro(Mean, float);
  vtkGetMacro(Variance, float);
  
 protected:
  vtkmpjVectorStreamlineStatistics();
  ~vtkmpjVectorStreamlineStatistics() {}
  
  // Usual data generation method
  void Execute();
  
  int NumberOfFibers;
  float Minimum;
  float Maximum;
  float Variance;
  float Mean;
 
private:
  vtkmpjVectorStreamlineStatistics(const vtkmpjVectorStreamlineStatistics&);// Not implemented.
  void operator=(const vtkmpjVectorStreamlineStatistics&);  // Not implemented.
};

#endif

