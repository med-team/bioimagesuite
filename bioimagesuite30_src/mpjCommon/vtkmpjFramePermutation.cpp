//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------






#include "vtkObjectFactory.h"
#include "vtkmpjFramePermutation.h"
#include "vtkFloatArray.h"
#include "vtkMath.h"
#include "math.h"


//------------------------------------------------------------------------------
vtkmpjFramePermutation* vtkmpjFramePermutation::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjFramePermutation");
  if(ret)
    {
    return (vtkmpjFramePermutation*)ret;
    }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjFramePermutation;
}

// Construct object with no children.
vtkmpjFramePermutation::vtkmpjFramePermutation()
{
  this->Permutation = NULL;
}

// ----------------------------------------------------------------------------
vtkmpjFramePermutation::~vtkmpjFramePermutation()
{
  if (this->Permutation != NULL)
    this->Permutation->Delete();
}

// ----------------------------------------------------------------------------
void vtkmpjFramePermutation::SimpleExecute(vtkImageData* input, vtkImageData* output)
{

  if (this->GetInput()==NULL)
    {
      vtkErrorMacro(<<"No Input Image\n Exiting\n");
      return;
    }

  vtkDataArray* in=input->GetPointData()->GetScalars();
  vtkDataArray* out=output->GetPointData()->GetScalars();
  int numscalars=in->GetNumberOfTuples();

  int dim[3];  input->GetDimensions(dim);
  int nc=input->GetNumberOfScalarComponents();

  float pog = 0.0;
  int tenth=(int)(numscalars * nc / 10.0);
  
  int count = 0;
  for(int c=0; c<this->Permutation->GetNumberOfTuples(); c++) {
    int target = (int)this->Permutation->GetTuple1(c);
    //    fprintf(stderr,"%d to target=%d\n",c,target);
    for(int n=0;n<numscalars;n++) {
      out->SetComponent(n,c,in->GetComponent(n,target));
      
      count++;
      if (count==tenth) {
	pog+=0.1;
	this->UpdateProgress(pog);
	count=0;
      }
    }
  }

  this->UpdateProgress(1.0);
}
// ----------------------------------------------------------------------------

