//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





// 
// Creates an icosahedron of an arbitrary depth (frequency).
// With depth=1, it will have 20 faces and 12 vertices.
// 

#ifndef __vtkmpjIcosahedronSource_h
#define __vtkmpjIcosahedronSource_h

#include "vtkPolyDataSource.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkUnsignedCharArray.h"
#include "vtkFloatArray.h"

#define VTK_MPJ_MAX_DEPTH 100

class vtkmpjIcosahedronSource : public vtkPolyDataSource 
{
public:
  vtkTypeMacro(vtkmpjIcosahedronSource,vtkPolyDataSource);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Construct an icosahedron of depth=1
  static vtkmpjIcosahedronSource *New();

  // Description:
  // Set the depth of the sphere subdivision
  vtkSetClampMacro(Depth,int,1,VTK_MPJ_MAX_DEPTH);
  vtkGetMacro(Depth,int);

  // Description:
  // Set whether the list of points should contain duplicates
  vtkSetClampMacro(AllowDuplicatePoints,int,0,1);
  vtkBooleanMacro(AllowDuplicatePoints,int);
  vtkGetMacro(AllowDuplicatePoints,int);

protected:
  vtkmpjIcosahedronSource();
  ~vtkmpjIcosahedronSource() {}

  void Execute();
  
  int Depth;
  int AllowDuplicatePoints;
  
  void InitializePolys(vtkPoints *pts, vtkCellArray *polys, vtkUnsignedCharArray *scalars);
  void SubdividePolys(vtkPoints *newPoints, vtkCellArray *oldCells, vtkCellArray *newCells, vtkUnsignedCharArray *newScalars);
  void FindOrInsert1Point(vtkPoints *newPoints, vtkUnsignedCharArray *newScalars, float a[3], vtkIdType &ra);
  void FindOrInsert3Points(vtkPoints *newPoints, vtkUnsignedCharArray *newScalars, float a[3], float b[3], float c[3], vtkIdType &ra, vtkIdType &rb, vtkIdType &rc);
  
 private:    
  
  vtkIdType **conn;
  
  vtkmpjIcosahedronSource(const vtkmpjIcosahedronSource&);  // Not implemented.
  void operator=(const vtkmpjIcosahedronSource&);  // Not implemented.
};

#endif



