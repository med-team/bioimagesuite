//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------






#include "vtkObjectFactory.h"
#include "vtkmpjStreamlineQuantization.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkFloatArray.h"

//------------------------------------------------------------------------------
vtkmpjStreamlineQuantization* vtkmpjStreamlineQuantization::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjStreamlineQuantization");
  if(ret)
    {
      return (vtkmpjStreamlineQuantization*)ret;
    }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjStreamlineQuantization;
}

// ----------------------------------------------------------------------------
vtkmpjStreamlineQuantization::vtkmpjStreamlineQuantization()
{
  this->Image = NULL;
  this->MaskValue = 1;
  this->BackgroundValue = 0;
} 

vtkmpjStreamlineQuantization::~vtkmpjStreamlineQuantization()
{
  if (this->Image != NULL)
    this->Image->Delete();
} 

// ----------------------------------------------------------------------------
void vtkmpjStreamlineQuantization::Execute()
{ 
  vtkPolyData *input=(vtkPolyData *)this->GetInput();
  //  vtkDataSet *output=this->GetOutput();

  vtkPointData *pd=this->Image->GetPointData();
  vtkDataArray *out=pd->GetScalars();

  vtkCellArray *inLines = NULL;

  vtkPoints *inPts;
  vtkIdType numPts = 0;
  vtkIdType numLines;

  vtkIdType npts=0, *pts=NULL;
  int abort=0;
  vtkIdType inCellId;
  
  float val;

  // zero out background
  out->FillComponent(0, this->BackgroundValue);

  long count = 0;

  // Check input and initialize
  //  
  if ( !(inPts=input->GetPoints()) || 
       (numPts = inPts->GetNumberOfPoints()) < 1 ||
       !(inLines = input->GetLines()) || 
       (numLines = inLines->GetNumberOfCells()) < 1 )
    {
      return;
    }

  for (inCellId=0, inLines->InitTraversal(); 
       inLines->GetNextCell(npts,pts) && !abort; inCellId++)
    {
      this->UpdateProgress((float)inCellId/numLines);
      abort = this->GetAbortExecute();
      
      if (npts < 2)
	{
	  vtkWarningMacro(<< "Less than two points in line!");
	  continue; //skip filtering this line
	}
      
      int index=-1;
      
      for(int i=1;i<npts;i++)
	{
	  double x1[3],x2[3],p[3],d[3],grad[3];
	  double maxdist=0.0;
	  int maxindex=0;
	  int k;
	  
	  inPts->GetPoint(pts[i-1],x1);
	  inPts->GetPoint(pts[i], x2);
	  
	  for (k=0;k<=2;k++)
	    {
	      d[k]=x2[k]-x1[k];
	      if (fabs(d[k])>fabs(maxdist))
		{
		  maxdist=d[k];
		  maxindex=k;
		}
	    }
	  
	  for (k=0;k<=2;k++)
	    {
	      if (d[k]!=0.0)
		grad[k]=d[k]/maxdist;
	      else
		grad[k]=0.0;
	    }
      
	  for (float dt=0.0;dt<fabs(maxdist);dt+=0.25)
	    {
	      if (maxdist==0)
		{
		  for (k=0;k<=2;k++)
		    p[k]=x1[k];
		}
	      else
		{
		  for (k=0;k<=2;k++)
		    p[k]=x1[k]+(dt/fabs(maxdist))*d[k];
		}
	      
	      //trans->TransformPoint(p,tx);

	      // find point location
	      index = this->Image->FindPoint(p);
	      
	      if (index >= 0) {
		val = out->GetComponent(index, 0);
		if (val != this->MaskValue) {
		  count++;
		  out->SetComponent(index, 0, this->MaskValue);
		}
	      }
	      
	    }
	}
    }

  this->NumberOfVoxels = count;
}

// ----------------------------------------------------------------------------

