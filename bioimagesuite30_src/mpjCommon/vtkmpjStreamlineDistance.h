//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#ifndef __vtkmpjStreamlineDistance_h
#define __vtkmpjStreamlineDistance_h

#include "vtkPolyDataToPolyDataFilter.h"
#include "vtkDataSet.h"
#include "vtkPolyData.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDataArray.h"
#include "vtkFloatArray.h"
#include "vtkPointData.h"
#include "vtkPoints.h"

class vtkmpjStreamlineDistance : public vtkPolyDataToPolyDataFilter
{
public:
  vtkTypeMacro(vtkmpjStreamlineDistance,vtkPolyDataToPolyDataFilter);

  // Description:
  // Construct object 
  static vtkmpjStreamlineDistance *New();
  
  void SetReference(vtkDataSet *input); 
  void SetTarget(vtkDataSet *input);
  vtkPolyData *GetInput(int index);

  // Get/Set reference polyline
  vtkGetMacro(ReferenceIndex, int);
  vtkSetMacro(ReferenceIndex, int);

  // Get/Set target polyline
  vtkGetMacro(TargetIndex, int);
  vtkSetMacro(TargetIndex, int);
  
  // Get/Set stats
  vtkGetMacro(Mean, float);
  vtkGetMacro(Minimum, float);
  vtkGetMacro(Maximum, float);

 protected:
  vtkmpjStreamlineDistance();
  ~vtkmpjStreamlineDistance() {}
  
  // Usual data generation method
  void Execute();
  
  int ReferenceIndex;
  int TargetIndex;
  float Mean;
  float Minimum;
  float Maximum;
  
 private:
  vtkmpjStreamlineDistance(const vtkmpjStreamlineDistance&);// Not implemented.
  void operator=(const vtkmpjStreamlineDistance&);  // Not implemented.
};

#endif

