//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------


/* Computes the Hessian image */

#ifndef __vtkmpjImageHessian_h
#define __vtkmpjImageHessian_h

#include "vtkImageData.h"
#include "vtkpxSimpleImageToImageFilter.h"

class vtkmpjImageHessian : public vtkpxSimpleImageToImageFilter
{
public:
  static vtkmpjImageHessian *New();
  vtkTypeMacro(vtkmpjImageHessian,vtkpxSimpleImageToImageFilter);

  // Description:
  // Set Scale Sigma
  vtkGetMacro(Sigma,float);
  vtkSetMacro(Sigma,float);

  // Description:
  // Sets/Gets mask where hessian is to be computed
  vtkGetObjectMacro(Mask, vtkImageData);
  vtkSetObjectMacro(Mask, vtkImageData);

  // Description:
  // Set/Get Enable Gaussian Smoothing
  vtkGetMacro(EnableSmoothing, int);
  vtkSetMacro(EnableSmoothing, int);
  vtkBooleanMacro(EnableSmoothing, int);

  // Description:
  // Set/Get Enable Thresholding
  vtkGetMacro(EnableThresholding, int);
  vtkSetMacro(EnableThresholding, int);
  vtkBooleanMacro(EnableThresholding, int);

  // Description:
  // Set Scale Sigma
  vtkGetMacro(ThresholdValue,float);
  vtkSetMacro(ThresholdValue,float);

  // Description:
  // Set/Get Append Sigma
  vtkGetMacro(AppendSigmaImage, int);
  vtkSetMacro(AppendSigmaImage, int);
  vtkBooleanMacro(AppendSigmaImage, int);

protected:

  //BTX
  vtkmpjImageHessian();
  virtual ~vtkmpjImageHessian();
  vtkmpjImageHessian(const vtkmpjImageHessian&) {};
  void operator=(const vtkmpjImageHessian&) {};
  //ETX
  virtual void SimpleExecute(vtkImageData* input,vtkImageData* output);
  virtual void ExecuteInformation();

  vtkImageData *Mask;

  float Sigma;
  float ThresholdValue;
  int EnableSmoothing;
  int EnableThresholding;
  int AppendSigmaImage;

};

#endif



