//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------






#include "vtkObjectFactory.h"
#include "vtkmpjImageVesselMaximum.h"

//------------------------------------------------------------------------------
vtkmpjImageVesselMaximum* vtkmpjImageVesselMaximum::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjImageVesselMaximum");
  if(ret)
    {
      return (vtkmpjImageVesselMaximum*)ret;
    }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjImageVesselMaximum;
}

// Construct object with no children.
vtkmpjImageVesselMaximum::vtkmpjImageVesselMaximum()
{
}

// ----------------------------------------------------------------------------
vtkmpjImageVesselMaximum::~vtkmpjImageVesselMaximum()
{
}

// ----------------------------------------------------------------------------
void vtkmpjImageVesselMaximum::AddInput(vtkImageData *input)
{
  this->vtkProcessObject::AddInput(input);
}

//----------------------------------------------------------------------------
void vtkmpjImageVesselMaximum::SetInput(int idx, vtkImageData *input)
{
  this->vtkProcessObject::SetNthInput(idx, input);
}

//----------------------------------------------------------------------------
vtkImageData *vtkmpjImageVesselMaximum::GetInput()
{
  return this->GetInput(0);
}

//----------------------------------------------------------------------------
vtkImageData *vtkmpjImageVesselMaximum::GetInput(int idx)
{
  if (this->NumberOfInputs <= idx)
    {
      return NULL;
    }
  
  return (vtkImageData*)(this->Inputs[idx]);
}

// ----------------------------------------------------------------------------
void vtkmpjImageVesselMaximum::ExecuteInformation()
{
  if (this->NumberOfInputs==0 || this->GetInput(0)==NULL) {
    vtkErrorMacro(<<"No Input Image Data !!");
    return;
  }
  
  this->vtkpxSimpleImageToImageFilter::ExecuteInformation();  
}

// ----------------------------------------------------------------------------

void vtkmpjImageVesselMaximum::SimpleExecute(vtkImageData *input, vtkImageData *output)
{
  int dim_0[3];
  this->GetInput(0)->GetDimensions(dim_0);

  for (int i=1;i<this->NumberOfInputs;i++)
    {
      if (this->GetInput(i)==NULL)
	{
	  vtkErrorMacro(<<"No Input Image Data " << i << " !!");
	  return;
	}
      else
	{
	  int dim_1[3];
	  this->GetInput(i)->GetDimensions(dim_1);
	  int sum=0;
	  for (int kk=0;kk<=2;kk++)
	    sum+=abs(dim_1[kk]-dim_0[kk]);
	  if (sum!=0)
	    {
	      vtkErrorMacro(<<"Images have different Dimensions !!");
	      return;
	    }
	}
    }
  
  vtkDataArray* out=output->GetPointData()->GetScalars();  

  int numscalars=out->GetNumberOfTuples();
  int numcomp=output->GetNumberOfScalarComponents();
  int nc = this->GetInput(0)->GetNumberOfScalarComponents();
  
  int count = 0;
  float pog = 0.0;
  int tenth= (int)(numscalars / 10.0);
  
  this->UpdateProgress(0.01);
  
  vtkDataArray *in0 = this->GetInput(0)->GetPointData()->GetScalars();
  
  for(int n=0; n<numscalars; n++) 
    {
      double max = in0->GetComponent(n, 0);    
      int sermax = 0;
      for(int series=1; series<this->NumberOfInputs; series++) 
	{            
	  double v = this->GetInput(series)->GetPointData()->GetScalars()->GetComponent(n, 0);      
	  
	  if (v > max) 
	    {
	      max=v; 
	      sermax=series;   
	    }
	}
    
      // save maximum
      out->SetComponent(n, 0, max);
      
      if ( max > 0 ) 
	{
	  // save corresponding sigma
	  out->SetComponent(n, 1, this->GetInput(sermax)->GetPointData()->GetScalars()->GetComponent(n,1));
      
	  // save tensor
	  for(int i=2; i<8; i++) 
	    out->SetComponent(n, i, this->GetInput(sermax)->GetPointData()->GetScalars()->GetComponent(n,i));
	} 
      else 
	{
	  // save sigma
	  out->SetComponent(n, 1, 0.0);
	  // save tensor
	  for(int i=2; i<8; i++) 
	    out->SetComponent(n, i, 0.0);
	}

      count++;
      if (count==tenth)
	{
	  pog+=0.1;
	  this->UpdateProgress(pog);
	  count=0;
	}
    }
  
  this->UpdateProgress(1.0);
}


