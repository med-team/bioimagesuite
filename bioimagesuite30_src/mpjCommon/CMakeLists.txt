#BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
#BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
#BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
#BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
#BIOIMAGESUITE_LICENSE  Medicine, http:#www.bioimagesuite.org.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
#BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
#BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
#BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
#BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
#BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
#BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#BIOIMAGESUITE_LICENSE  See also  http:#www.gnu.org/licenses/gpl.html
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
#BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
#BIOIMAGESUITE_LICENSE 
#BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#
# Source files
#
#  Modified
#
# Here is where you can add the name of your local common classes.
#
SET(KIT vtkmpjCommon)

INCLUDE_DIRECTORIES(${BIOIMAGESUITE3_SOURCE_DIR}/Common)
INCLUDE_DIRECTORIES(${BIOIMAGESUITE3_SOURCE_DIR}/mpjCommon)

SET (KITSRCS
    vtkmpjFrameExtract.cpp
    vtkmpjFrameAppend.cpp
    vtkmpjFrameAverage.cpp
    vtkmpjFramePermutation.cpp
    vtkmpjImageInterleaving.cpp
    vtkmpjImageTwoNorm.cpp
    vtkmpjImageStatistics.cpp
    vtkmpjPriorityQueue.cpp
    vtkmpjTestOptimizer.cpp
    vtkmpjTensorStreamline.cpp
    vtkmpjImageBoundary.cpp
    vtkmpjTensorStreamlineRegionFilter.cpp
    vtkmpjTensorStreamlineStatistics.cpp
    vtkmpjStreamlineQuantization.cpp
    vtkmpjImagePointSource.cpp
    vtkmpjScalarsToFieldData.cpp
    vtkmpjStreamer.cpp
    vtkmpjVectorStreamline.cpp
    vtkmpjImageSignalToNoiseRatio.cpp
    vtkmpjStreamlineDistance.cpp
    vtkmpjThresholdPolyData.cpp
    vtkmpjThresholdPolyDataByDistance.cpp
    vtkmpjVectorStreamlineStatistics.cpp
    vtkmpjImageHessian.cpp
    vtkmpjImageVesselEnhancement.cpp
    vtkmpjImageVesselMaximum.cpp
    vtkmpjDiffusionProfile.cpp
    vtkmpjTubeFilter.cpp
    vtkmpjVertexSource.cpp
    vtkmpjSphereSubdivisionSource.cpp
    vtkmpjIcosahedronSource.cpp
    vtkmpjImageT2Relax.cpp
)

SET_SOURCE_FILES_PROPERTIES(
vtkmpjStreamer
ABSTRACT
)

SET(KITLIBS
  vtkmpjCommon
  numerics
  vtkpxCommon
  vtkpxCommonTCL
  vtkCommonTCL
   ${BIOIMAGESUITE3_COMMON_LIBS}
)

SET(KITJAVALIBS
  vtkpxCommonJava
  vtkpxCommon
  numerics
   ${BIOIMAGESUITE3_COMMON_JAVALIBS}
)


# --------------------------------------------------------------------------
# You probably do not need to modify anything below this line

SET (KITEXTRAFILES)
SET (KITTCLSRCS )


bis_complex("mpjCommon" ${KIT} ${FILELIST})



