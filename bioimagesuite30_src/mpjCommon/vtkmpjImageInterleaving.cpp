//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------






#include "vtkObjectFactory.h"
#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkmpjImageInterleaving.h"
#include "vtkFloatArray.h"
#include "vtkMath.h"
#include "math.h"


//------------------------------------------------------------------------------
vtkmpjImageInterleaving* vtkmpjImageInterleaving::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjImageInterleaving");
  if(ret)
    {
    return (vtkmpjImageInterleaving*)ret;
    }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjImageInterleaving;
}

// Construct object with no children.
vtkmpjImageInterleaving::vtkmpjImageInterleaving()
{
  this->Interleaving = VTK_MPJ_FRAMEINTERLEAVE;
  this->NumberOfFrames = 1;
}
// ----------------------------------------------------------------------------
vtkmpjImageInterleaving::~vtkmpjImageInterleaving()
{
}
// ----------------------------------------------------------------------------

void vtkmpjImageInterleaving::ExecuteInformation()
{
  vtkImageData *input=this->GetInput();

  if (input==NULL)
    {
	vtkErrorMacro(<<"No Input Specified!!");
	return;
      }

  vtkImageData *output=this->GetOutput();
  vtkpxSimpleImageToImageFilter::ExecuteInformation();

  int dim[3];  input->GetDimensions(dim);
  int nc=input->GetNumberOfScalarComponents();

  if (this->Interleaving == VTK_MPJ_SLICEINTERLEAVE ) {    
    output->SetNumberOfScalarComponents(1);
    output->SetWholeExtent(0,dim[0]-1,0,dim[1]-1,0,dim[2]*nc-1);
  } else {    
    output->SetNumberOfScalarComponents(this->NumberOfFrames);
    output->SetWholeExtent(0,dim[0]-1,0,dim[1]-1,0,dim[2]/this->NumberOfFrames-1);
  }
    
}

// ----------------------------------------------------------------------------
void vtkmpjImageInterleaving::SimpleExecute(vtkImageData* input,vtkImageData* output)
{

  if (input==NULL)
    {
      vtkErrorMacro(<<"No Input Image\n Exiting\n");
      return;
    }

  vtkDataArray* in =input->GetPointData()->GetScalars();
  

  int dim[3];  input->GetDimensions(dim);
  int nc=input->GetNumberOfScalarComponents();

  output->CopyStructure(input);
  
  //  if (this->Interleaving == VTK_MPJ_SLICEINTERLEAVE ) {    
  // output->SetNumberOfScalarComponents(this->NumberOfFrames);
  // output->SetWholeExtent(0,dim[0]-1,0,dim[1]-1,0,dim[2]nc-1);
  // output->SetDimensions(dim[0],dim[1],dim[2]*nc);
  //} else {    
  output->SetNumberOfScalarComponents(this->NumberOfFrames);
  output->SetWholeExtent(0,dim[0]-1,0,dim[1]-1,0,dim[2]/this->NumberOfFrames-1);
  output->SetDimensions(dim[0],dim[1],dim[2]/this->NumberOfFrames);
  //}
  
  output->AllocateScalars();
  vtkDataArray* out=output->GetPointData()->GetScalars();
  
  if (this->Interleaving == VTK_MPJ_SLICEINTERLEAVE) {
    // VTK_MPJ_SLICEINTERLEAVE    
    
    for (int k=0;k<dim[2];k++) {
      int offset = k*dim[0]*dim[1];
      int noffset = (k / this->NumberOfFrames)*dim[0]*dim[1];
      int index = 0;
      for (int j=0;j<dim[1];j++)
	for (int i=0;i<dim[0];i++)
	  {		    
	    out->SetComponent(noffset+index, k % this->NumberOfFrames, 
			      in->GetComponent(offset+index,0));
	    ++index;
	  }
    }
    
  } else {
    
    int count = 0;
    int size = dim[0]*dim[1]*(dim[2] / this->NumberOfFrames);    
    for (int k=0;k<dim[2];k++) { 
      for (int j=0;j<dim[1];j++)
	for (int i=0;i<dim[0];i++)
	  {
	    out->SetComponent(count % size,count / size,in->GetComponent(count,0));
	    count++;
	  }
    }
    
    // VTK_MPJ_FRAMEINTERLEAVE
  }
  
  this->UpdateProgress(1.0);
}
// ----------------------------------------------------------------------------

