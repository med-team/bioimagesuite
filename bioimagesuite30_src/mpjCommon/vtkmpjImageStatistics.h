//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------






#ifndef __vtkmpjImageStatistics_h
#define __vtkmpjImageStatistics_h

#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkImageData.h"
#include "vtkFloatArray.h"
#include "vtkPointData.h"

class vtkmpjImageStatistics : public vtkpxSimpleImageToImageFilter
{
 public:
  static vtkmpjImageStatistics *New();
  vtkTypeMacro(vtkmpjImageStatistics,vtkpxSimpleImageToImageFilter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get single-region statistics information for the data
  vtkGetMacro(Min, double);
  vtkGetMacro(Max, double);
  vtkGetMacro(Mean, double);
  vtkGetMacro(Variance, double);
  vtkGetMacro(VoxelCount, long int);
  
  // Description::
  // Get/Set Number of regions in the mask
  vtkGetMacro(NumberOfRegions,int);
  vtkSetMacro(NumberOfRegions,int);
  
  // Description: 
  // Sets/Gets mask for computing statistics.
  // If a mask is not provided, tensor is
  // computed for every voxel in the image.
  vtkSetObjectMacro(Mask, vtkImageData);
  vtkGetObjectMacro(Mask, vtkImageData);
  
  // Gets region statistics
  double GetRegionMax(int index) { return this->RegionMax[index]; }
  double GetRegionMin(int index) { return this->RegionMin[index]; }
  double GetRegionMean(int index) { return this->RegionMean[index]; }
  double GetRegionVariance(int index) { return this->RegionVariance[index]; }
  long GetRegionVoxelCount(int index) { return this->RegionVoxelCount[index]; }

 protected:
  int NumberOfRegions;  
  vtkFloatArray *RegionValues;
  vtkImageData *Mask;

  double *RegionMax;
  double *RegionMin;
  double *RegionMean;
  double *RegionVariance;
  double *RegionSum;
  double *RegionSumSqr;
  long *RegionVoxelCount;

  double Min;
  double Max;
  double Mean;
  double Variance;
  long int VoxelCount;
  
  vtkmpjImageStatistics();
  virtual ~vtkmpjImageStatistics();
  
  virtual void SimpleExecute(vtkImageData* in, vtkImageData* out);
  //  virtual void ExecuteInformation();

 private:
  vtkmpjImageStatistics(const vtkmpjImageStatistics&); // Not implemented
  void operator=(const vtkmpjImageStatistics&); // Not implemented
};

#endif



