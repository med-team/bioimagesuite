//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#include "vtkmpjVertexSource.h"
#include "vtkPointSet.h"

vtkStandardNewMacro(vtkmpjVertexSource);

// Construct object with scaling on and scale factor 1.0. 
vtkmpjVertexSource::vtkmpjVertexSource()
{
  this->NumberOfPoints = 0;
  this->Points = NULL;
  this->ScaleFactor = 1.0;
}

vtkmpjVertexSource::~vtkmpjVertexSource()
{
  if (this->Points != NULL)
    this->Points->Delete();
}

void vtkmpjVertexSource::Execute()
{
  double *pt, x[3];
  vtkIdType i;

  vtkPoints *newPoints;
  vtkCellArray *newVerts;
  vtkPolyData *output = this->GetOutput();

  if (this->Points == NULL) 
    return;
  
  int numpts = this->Points->GetNumberOfPoints();
 
  newPoints = vtkPoints::New();
  newPoints->Allocate(numpts);
  newVerts = vtkCellArray::New();
  newVerts->Allocate(newVerts->EstimateSize(1,numpts));
  
  newVerts->InsertNextCell(numpts);
  
  for (i=0; i<numpts; i++)
    {
      pt = this->Points->GetPoint(i);
      
      x[0] = pt[0] * this->ScaleFactor;
      x[1] = pt[1] * this->ScaleFactor;
      x[2] = pt[2] * this->ScaleFactor;
      
      newVerts->InsertCellPoint(newPoints->InsertNextPoint(x));
    }
  
  //
  // Update ourselves and release memory
  //
  output->SetPoints(newPoints);
  newPoints->Delete();
  
  output->SetVerts(newVerts);
  newVerts->Delete();
}

void vtkmpjVertexSource::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  
  os << indent << "Scale Factor: " << this->ScaleFactor << "\n";
}

