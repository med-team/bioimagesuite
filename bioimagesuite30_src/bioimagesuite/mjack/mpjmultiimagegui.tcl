#BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
#BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
#BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
#BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
#BIOIMAGESUITE_LICENSE  Medicine, http:#www.bioimagesuite.org.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
#BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
#BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
#BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
#BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
#BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
#BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#BIOIMAGESUITE_LICENSE  See also  http:#www.gnu.org/licenses/gpl.html
#BIOIMAGESUITE_LICENSE  
#BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
#BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
#BIOIMAGESUITE_LICENSE 
#BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------

package provide mpjmultiimagegui 1.0

package require pxitclimage 1.0





# ----------------------------------------------------------
#  GUI for multiple image object
# ----------------------------------------------------------
itcl::class mpjmultiimageGUI {

    inherit mpjmultiobjectGUI

    constructor { } {
	mpjmultiobjectGUI::constructor
    } {
    }
    
    destructor { }
    
    public    method Initialize { basewidg } 

    public    method AddImage { descr }
    public    method SetImage { index } 
    public    method GetImage { index } { return [ GetObject $index ] }
    public    method GetCurSelection { } 
    public    method DeleteImage { index } { DeleteObject $index }

    public    method SetDisplayCommand { cmd }

    protected method CreateNewObject { }
    protected method CreateNewObjectGUI { widget }

}

# ----------------------------------------------------------
itcl::body mpjmultiimageGUI::CreateNewObject { } {

    return  [ [  pxitclimage \#auto ] GetThisPointer ]
}

itcl::body mpjmultiimageGUI::CreateNewObjectGUI { widget } {
    set itclimageGUI [ [ pxitclimageGUI \#auto ] GetThisPointer ]
    pack [ $itclimageGUI Initialize $widget.[ pxvtable::vnewobj ] ] -side left -fill x
    return $itclimageGUI
}

itcl::body mpjmultiimageGUI::AddImage { descr } {
    
    set newobj [ $this AddObject $descr ]
    $this SetImage [ expr [ llength $itclobjectlist ] -1 ]    
    return $newobj
}

itcl::body mpjmultiimageGUI::SetImage { index } {

    $this SetObject $index
    $objectGUI configure -description [ $listwidget get $currentobject ]
    $objectGUI Update
}

itcl::body mpjmultiimageGUI::SetDisplayCommand { cmd } {
    $objectGUI configure -displaycommand $cmd
}

itcl::body mpjmultiimageGUI::GetCurSelection { } {    
    return [ $listwidget index [ $listwidget get ]]
}

itcl::body mpjmultiimageGUI::Initialize { basewidg } {
    
    if { $initialized == 1 } { 
	return $basewidget 
    }
    
    mpjmultiobjectGUI::Initialize $basewidg

    #$listbox configure -selectmode single
    $listwidget configure -command [itcl::code $this SetImage -1]
    
    set initialized 1
    
    return $basewidget
}
 




