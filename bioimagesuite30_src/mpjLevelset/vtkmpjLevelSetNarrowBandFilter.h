//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





/* 
  -- vtkmpjLevelSetNarrowBandFilter.h --
  
  Given an input level set, creates a narrow band of a given size.
  Returns an image mask of the narrow band as well as a list of
  locations and level set values.
  
  Marcel Jackowski, June 2003.
*/

#ifndef __vtkmpjLevelSetNarrowBandFilter_h
#define __vtkmpjLevelSetNarrowBandFilter_h

#include "vtkObjectFactory.h"
#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkImageData.h"
#include "vtkPointData.h"
#include "vtkmpjPriorityQueue.h"
#include "vtkImageAccumulate.h"
#include "vtkFloatArray.h"
#include "vtkIntArray.h"
#include "vtkMath.h"
#include "math.h"

class vtkmpjLevelSetNarrowBandFilter : public vtkpxSimpleImageToImageFilter
{
 public: 
  static vtkmpjLevelSetNarrowBandFilter *New();
  vtkTypeMacro(vtkmpjLevelSetNarrowBandFilter,vtkpxSimpleImageToImageFilter);
  
  // Set/Get narrow bandwidth 
  vtkGetMacro(NarrowBandwidth,float);
  vtkSetMacro(NarrowBandwidth,float);

  // Set/Get level set to extract
  vtkGetMacro(LevelSetValue, float);
  vtkSetMacro(LevelSetValue, float);

  // Get extracted narrow band points
  vtkGetObjectMacro(NarrowBandPoints, vtkFloatArray);

 protected:
  vtkFloatArray *NarrowBandPoints;
  float NarrowBandwidth; // default 12
  float LevelSetValue;   // default 0

  vtkmpjLevelSetNarrowBandFilter();
  virtual ~vtkmpjLevelSetNarrowBandFilter();
  
  void ExecuteInformation();
  
  virtual void SimpleExecute(vtkImageData *input, vtkImageData *output);
  
 private:
  //BTX
  float pog;
  int count, tenth;
  //ETX

  vtkmpjLevelSetNarrowBandFilter(const vtkmpjLevelSetNarrowBandFilter&);// Not implemented
  void operator=(const vtkmpjLevelSetNarrowBandFilter&); // Not Implemented
};

#endif

