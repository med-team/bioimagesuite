//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#include "vtkmpjLevelSetNarrowBandFilter.h"

//------------------------------------------------------------------------------
vtkmpjLevelSetNarrowBandFilter* vtkmpjLevelSetNarrowBandFilter::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjLevelSetNarrowBandFilter");
  if (ret) {
    return (vtkmpjLevelSetNarrowBandFilter*)ret;
  }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjLevelSetNarrowBandFilter;
}

// Construct object with no children.
vtkmpjLevelSetNarrowBandFilter::vtkmpjLevelSetNarrowBandFilter()
{
  this->NarrowBandwidth = 12;
  this->LevelSetValue = 0;
  this->NarrowBandPoints = NULL;
}

// ----------------------------------------------------------------------------
vtkmpjLevelSetNarrowBandFilter::~vtkmpjLevelSetNarrowBandFilter()
{
}

// ----------------------------------------------------------------------------
void vtkmpjLevelSetNarrowBandFilter::ExecuteInformation()
{
  this->vtkpxSimpleImageToImageFilter::ExecuteInformation();
}

// ----------------------------------------------------------------------------
void vtkmpjLevelSetNarrowBandFilter::SimpleExecute(vtkImageData *input, vtkImageData *output)
{
  int dim[3]; input->GetDimensions(dim);
  int dim0Tdim1 = dim[0]*dim[1];
  //  int factor[3] = {1, dim[0], dim0Tdim1};
  float maxwidth = this->NarrowBandwidth / 2.0;
  
  vtkDataArray *in = input->GetPointData()->GetScalars();
  vtkDataArray *out = output->GetPointData()->GetScalars();

  int total = in->GetNumberOfTuples();
  int coord[3];

  if (this->NarrowBandPoints != NULL) {
    this->NarrowBandPoints->Delete();
  }
  this->NarrowBandPoints = vtkFloatArray::New();
  this->NarrowBandPoints->SetNumberOfComponents(4);

  this->pog = 0.0;  
  this->tenth=(int)(total / 10.0);
  this->count = 0;
  
  this->UpdateProgress(0.01);
  
  for (int index=0; index<total; index++) {
    
    if (this->count==this->tenth)
      {
	this->pog+=0.1;
	this->UpdateProgress(this->pog);
	this->count=0;
      }
    
    // decompose linear index into point coordinates
    coord[2] = index / dim0Tdim1; 
    coord[1] = (index % dim0Tdim1) / dim[0];
    coord[0] = (index % dim0Tdim1) % dim[0];
    
    // get level set value
    float value = in->GetComponent(index, 0);
    
    // keep narrow band values
    if ((value >= -maxwidth) && (value <= maxwidth)) {     
      
      out->SetComponent(index, 0, 1.0);     
      
      this->NarrowBandPoints->InsertNextTuple4((float)coord[0], (float)coord[1], 
					       (float)coord[2], (float)value);
    }
    
  }

  this->UpdateProgress(1.0);
}
// ----------------------------------------------------------------------------

