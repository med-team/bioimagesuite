//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





/* 
  -- vtkmpjFastMarchingExtension.h --
   
  Marcel Jackowski, June 2003
*/

#define VTK_MPJ_FROZEN       -1     
#define VTK_MPJ_NARROW_BAND   0
#define VTK_MPJ_UNVISITED    +1

#ifndef __vtkmpjFastMarchingExtension_h
#define __vtkmpjFastMarchingExtension_h

#include "vtkObjectFactory.h"
#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkImageData.h"
#include "vtkPointData.h"
#include "vtkmpjPriorityQueue.h"
#include "vtkImageAccumulate.h"
#include "vtkFloatArray.h"
#include "vtkIntArray.h"
#include "vtkMath.h"
#include "math.h"

class vtkmpjFastMarchingExtension : public vtkpxSimpleImageToImageFilter
{
 public: 
  static vtkmpjFastMarchingExtension *New();
  vtkTypeMacro(vtkmpjFastMarchingExtension,vtkpxSimpleImageToImageFilter);
  
  // Description: Sets speed image
  void SetSpeedImage(vtkImageData *speed);

  // Description: Set/Get extension image
  vtkSetObjectMacro(ExtensionImage, vtkImageData);
  vtkGetObjectMacro(ExtensionImage, vtkImageData);

  // Description:
  // Set/Get boundary conditions for the propagation
  vtkSetObjectMacro(KnownPoints, vtkFloatArray);
  vtkGetObjectMacro(KnownPoints, vtkFloatArray);

  // Description:
  // Turn on/off values for known points
  vtkGetMacro(KnownValues,int);
  vtkSetClampMacro(KnownValues,int,0,1);
  vtkBooleanMacro(KnownValues,int);

  // Description:
  // Set/Get maximum propagation time
  vtkGetMacro(PropagationTime,float);
  vtkSetMacro(PropagationTime,float);
  
  // Description:
  // Set/Get grid spacing
  vtkGetMacro(Spacing,float);
  vtkSetMacro(Spacing,float);
  
  // Front label image. Points interior to the front have value -1, points 
  // on the front boundary have value 0, and points outside have value +1.
  vtkGetObjectMacro(LabelImage, vtkImageData);
  
  // Return an array containing front point vertices and their values
  vtkGetObjectMacro(FrontPoints, vtkFloatArray);
  
 protected:
  float Spacing;
  float PropagationTime;
  int KnownValues;
  vtkImageData *Speed;
  vtkImageData *LabelImage;
  vtkImageData *ExtensionImage;
  vtkFloatArray *KnownPoints;
  vtkFloatArray *FrontPoints;

  vtkDataArray *label;
  vtkmpjPriorityQueue *queue;     

  vtkmpjFastMarchingExtension();
  virtual ~vtkmpjFastMarchingExtension();
  
  void ExecuteInformation();  
  virtual void SimpleExecute(vtkImageData *input, vtkImageData *output);
  virtual void InitializeFront(vtkImageData *input, vtkImageData *output);
  virtual void PropagateFront(vtkImageData *input, vtkImageData *output);
  virtual void SaveFrontPoints(vtkImageData *input);  
  virtual double ComputeSpeed(vtkImageData *input, vtkImageData *output, int coord[3], int index);
  virtual double ComputeFirstOrder(vtkImageData *input, vtkImageData *output, int coord[3], int index);

  int GetNeighborIndex(int j);
  void SortTerms(double term[3]);
  double Solve2(double a, double b, double rhs);
  double SolveN(int number, double term[3], double rhs);
  double SolveQuadratic(int number, double term[3], double rhs);
  
  double ComputeTime(vtkImageData *input, vtkImageData *output,
		     int coord[3], int index);  
 private:
  //BTX
  float pog;
  int count, tenth; 
  int neighbor_index[3];
  //ETX
  
  vtkmpjFastMarchingExtension(const vtkmpjFastMarchingExtension&);// Not implemented
  void operator=(const vtkmpjFastMarchingExtension&); // Not Implemented
};

#endif

