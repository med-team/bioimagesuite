//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#ifndef __vtkmpjImageCharacteristics_h
#define __vtkmpjImageCharacteristics_h

#include "vtkObjectFactory.h"
#include "vtkpxSimpleImageToImageFilter.h"
#include "vtkFloatArray.h"
#include "vtkImageData.h"
#include "vtkPointData.h"
#include "vtkMath.h"
#include "math.h"

class vtkmpjImageCharacteristics : public vtkpxSimpleImageToImageFilter
{
 public: 
  static vtkmpjImageCharacteristics *New();
  vtkTypeMacro(vtkmpjImageCharacteristics,vtkpxSimpleImageToImageFilter);
  
  // Description: 
  // Sets/Gets mask for determining propagation extents
  vtkSetObjectMacro(Mask, vtkImageData);
  vtkGetObjectMacro(Mask, vtkImageData);
  
  // Sets/Gets tensor information
  vtkSetObjectMacro(Tensor, vtkImageData);
  vtkGetObjectMacro(Tensor, vtkImageData);

  // Sets/Gets gradient of u-map
  vtkSetObjectMacro(Gradient, vtkImageData);
  vtkGetObjectMacro(Gradient, vtkImageData);

  vtkSetMacro(CoefficientA, float);
  vtkGetMacro(CoefficientA, float);
  
  vtkSetMacro(CoefficientB, float);
  vtkGetMacro(CoefficientB, float);
  
  vtkSetMacro(CoefficientC, float);
  vtkGetMacro(CoefficientC, float);

 protected:
  vtkImageData *Mask;
  vtkImageData *Tensor;
  vtkImageData *Gradient;
  float CoefficientA;
  float CoefficientB;
  float CoefficientC;
  vtkmpjImageCharacteristics();
  virtual ~vtkmpjImageCharacteristics();
  
  void Jacobi3x3(float a[3][3], float w[3], float v[3][3]);
  void NormalizeTensor3x3(float T[3][3], float w[3], float V[3][3], float N[3][3]);
  void FindEllipsoidPoint(float n[3], float w[3], float T[3][3], float N[3][3], float V[3][3], float FA);
  void ComputeCharacteristics(vtkImageData *input, vtkImageData *output, int coord[3], int index);
  void ExecuteInformation();
  virtual void SimpleExecute(vtkImageData *input, vtkImageData *output);
  int InsideMask(int index);
  
 private: 
  //BTX
  vtkDataArray *mask;
  //ETX
  vtkmpjImageCharacteristics(const vtkmpjImageCharacteristics&);// Not implemented
  void operator=(const vtkmpjImageCharacteristics&); // Not Implemented
};

#endif

