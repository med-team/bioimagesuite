//BIOIMAGESUITE_LICENSE  ---------------------------------------------------------------------------------
//BIOIMAGESUITE_LICENSE  This file is part of the BioImage Suite Software Package.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  X. Papademetris, M. Jackowski, N. Rajeevan, H. Okuda, R.T. Constable, and L.H
//BIOIMAGESUITE_LICENSE  Staib. BioImage Suite: An integrated medical image analysis suite, Section
//BIOIMAGESUITE_LICENSE  of Bioimaging Sciences, Dept. of Diagnostic Radiology, Yale School of
//BIOIMAGESUITE_LICENSE  Medicine, http://www.bioimagesuite.org.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is free software; you can redistribute it and/or
//BIOIMAGESUITE_LICENSE  modify it under the terms of the GNU General Public License version 2
//BIOIMAGESUITE_LICENSE  as published by the Free Software Foundation.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  This program is distributed in the hope that it will be useful,
//BIOIMAGESUITE_LICENSE  but WITHOUT ANY WARRANTY; without even the implied warranty of
//BIOIMAGESUITE_LICENSE  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//BIOIMAGESUITE_LICENSE  GNU General Public License for more details.
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  You should have received a copy of the GNU General Public License
//BIOIMAGESUITE_LICENSE  along with this program; if not, write to the Free Software
//BIOIMAGESUITE_LICENSE  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//BIOIMAGESUITE_LICENSE  See also  http://www.gnu.org/licenses/gpl.html
//BIOIMAGESUITE_LICENSE  
//BIOIMAGESUITE_LICENSE  If this software is modified please retain this statement and add a notice
//BIOIMAGESUITE_LICENSE  that it had been modified (and by whom).  
//BIOIMAGESUITE_LICENSE 
//BIOIMAGESUITE_LICENSE  -----------------------------------------------------------------------------------





#include "vtkmpjLevelSetTest.h"

//------------------------------------------------------------------------------
vtkmpjLevelSetTest* vtkmpjLevelSetTest::New()
{
  // First try to create the object from the vtkObjectFactory
  vtkObject* ret = vtkObjectFactory::CreateInstance("vtkmpjLevelSetTest");
  if (ret) {
    return (vtkmpjLevelSetTest*)ret;
  }
  // If the factory was unable to create the object, then create it here.
  return new vtkmpjLevelSetTest;
}

// Construct object with no children.
vtkmpjLevelSetTest::vtkmpjLevelSetTest()
{
  this->SpeedImage = NULL;
}

// ----------------------------------------------------------------------------
vtkmpjLevelSetTest::~vtkmpjLevelSetTest()
{
  if (this->SpeedImage != NULL)
    this->SpeedImage->Delete();
}

// ----------------------------------------------------------------------------
void vtkmpjLevelSetTest::ExecuteInformation()
{  
  this->vtkpxSimpleImageToImageFilter::ExecuteInformation();

  if (this->SpeedImage == NULL) {
    vtkErrorMacro(<<"no speed image!");
    return;
  }
}

// ----------------------------------------------------------------------------
double vtkmpjLevelSetTest::ComputeAdvection(vtkImageData *image, double v[3], 
					    int index, int coord[3])
{
  vtkDataArray *phi = image->GetPointData()->GetScalars();  
  int dim[3]; image->GetDimensions(dim);
  int dim0Tdim1 = dim[0]*dim[1];
  int factor[3] = {1, dim[0], dim0Tdim1};
  
  double advection = 0.0;    
  double value = image->GetPointData()->GetScalars()->GetComponent(index, 0);
  double diff_val = 0.0;
  
  for(int j=0; j<3; j++) {
    
    // compute backward differences
    if (coord[j] > 0) {      

      diff_val = value - phi->GetComponent(index-factor[j], 0);
      
      if (v[j] >= 0) {	
	advection += v[j] * diff_val;
      }
    }

    // compute forward differences
    if (coord[j] < dim[j]-1) {
      
      diff_val = phi->GetComponent(index+factor[j], 0) - value;
      
      if (v[j] < 0) {
	advection += v[j] * diff_val;
      }
    }
  }

  return advection;
}


// ----------------------------------------------------------------------------
double vtkmpjLevelSetTest::ComputeSpeed(vtkImageData *image, int index, int coord[3])
{
  vtkDataArray *speed = this->SpeedImage->GetPointData()->GetScalars();
  
  double v[3] ={1.0, 0.0, 0.0};
  
  //v[0] = speed->GetComponent(index, 0);
  //v[1] = speed->GetComponent(index, 1);
  //v[2] = speed->GetComponent(index, 2);  
  //(void)vtkMath::Normalize(v);  
  //double fa = speed->GetComponent(index, 0);  
  //printf("fa=%f\n",fa);  
  //  double entropy = ComputeEntropyGradientMagnitude(image, advection, index, coord);
  
  float normal[3];
  
  this->ComputeNormal(image, index, coord, normal);
  double dot = v[0]*normal[0] + v[1]*normal[1] + v[2]*normal[2];
  
  //double advection = ComputeAdvection(image, dot, index, coord);   
  
  //double entropy = ComputeEntropyGradientMagnitude(image, fabs(dot), index, coord);

  //  printf("entropy=%f\n",entropy); 

  return dot;

}



