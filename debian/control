Source: bioimagesuite
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: David Paleino <d.paleino@gmail.com>
Build-Depends: debhelper (>= 9), quilt, chrpath
Standards-Version: 3.9.3
Vcs-Browser: https://salsa.debian.org/med-team/bioimagesuite
Vcs-Git: https://salsa.debian.org/med-team/bioimagesuite.git
Homepage: http://www.bioimagesuite.org

Package: bioimagesuite
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, itk3.1, vtk-tcl, libmetakit-tcl,
 tcllib, iwidgets4, itcl3.1, libminc0, bioimagesuite-data, libxerces28,
 minc-tools, hdf5-tools, netcdf-bin, libbioimagesuite2
Description: integrated image analysis software suite
 BioImage Suite has extensive capabilities for both neuro/cardiac
 and abdominal image analysis and state of the art visualization.
 Many packages are available that are highly extensible, and provide
 functionality for image visualization and registration, surface
 editing, cardiac 4D multi-slice editing, diffusion tensor image
 processing, mouse segmentation and registration, and much more. It
 can be integrated with other biomedical image processing software,
 such as FSL and SPM. This site provides information, downloads,
 documentation, and other resources for users of the software.
 .
 BioImage Suite was developed at Yale University and has been
 extensively used at different labs at Yale since 2004.

Package: bioimagesuite-data
Architecture: all
Description: integrated image analysis software suite
 BioImage Suite has extensive capabilities for both neuro/cardiac
 and abdominal image analysis and state of the art visualization.
 Many packages are available that are highly extensible, and provide
 functionality for image visualization and registration, surface
 editing, cardiac 4D multi-slice editing, diffusion tensor image
 processing, mouse segmentation and registration, and much more. It
 can be integrated with other biomedical image processing software,
 such as FSL and SPM. This site provides information, downloads,
 documentation, and other resources for users of the software.
 .
 BioImage Suite was developed at Yale University and has been
 extensively used at different labs at Yale since 2004.
 .
 This package contains architecture-independent data files.

Package: libbioimagesuite2
Architecture: any
Section: libs
Depends: ${shlibs:Depends}
Description: integrated image analysis software suite
 BioImage Suite has extensive capabilities for both neuro/cardiac
 and abdominal image analysis and state of the art visualization.
 Many packages are available that are highly extensible, and provide
 functionality for image visualization and registration, surface
 editing, cardiac 4D multi-slice editing, diffusion tensor image
 processing, mouse segmentation and registration, and much more. It
 can be integrated with other biomedical image processing software,
 such as FSL and SPM. This site provides information, downloads,
 documentation, and other resources for users of the software.
 .
 BioImage Suite was developed at Yale University and has been
 extensively used at different labs at Yale since 2004.
 .
 This package contains library files.
